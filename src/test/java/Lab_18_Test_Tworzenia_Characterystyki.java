import confif.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Lab_18_Test_Tworzenia_Characterystyki extends SeleniumTestBase {

    @Test
    public void addCharacteristic(){
        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID().toString().substring(0, 10);
        String lsl = "8";
        String usl = "10";

        new LoginPage(driver)
                .typeEmail(new Config().getEmail())
                .typePassword(new Config().getPassword())
                .submitLogin()
                .goToCharacteristic()
                .clickAddCharacteristic()
                .selectProcess(processName)
                .typeName(characteristicName)
                .lowLimit(lsl)
                .upperLimit(usl)
                .clickCreateBtn()
                .assertCharacteristic(characteristicName,lsl,usl,"");

    }
}
