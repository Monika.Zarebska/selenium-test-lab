import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_11_Test_niepoprawnej_rejestracji extends SeleniumTestBase {

    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"@test"}};
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectRegistration(String wrongEmail) {

        new LoginPage(driver).register()
                .typeEmail(wrongEmail)
                .registerButtonClick()
                .invalidEmailRegistration();
    }
}
