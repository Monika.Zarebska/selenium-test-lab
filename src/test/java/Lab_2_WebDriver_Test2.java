import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Lab_2_WebDriver_Test2 {
        @Test
        public void playWithWebDriver () {
            System.setProperty("webdriver.chrome.driver", "c:/dev/driver/chromedriver.exe"); // ustawienie zmiennej srodowiskowej jak otworzyć przegladarke  z poziomu kodu musi byc dokladnie tak
            WebDriver driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); //czekamy na elementy maksymalnie 10 sek
            driver.manage().window().maximize();// driver. ctrl spacja i jest lista dostepnych metod na danym obiekcie
            driver.get("http://google.com");

            WebElement searchTxt = driver.findElement(By.cssSelector("input[name=q]"));//searchtxt to zmienna która mozna nazwac dowolnie. input to unikalny selektor (w chromie wyswietla sie ile jesttakich anie innych)
            searchTxt.sendKeys ("Quality Assurance");
            WebElement searchBtn = driver.findElement(By.cssSelector( "input[value='Szukaj w Google']"));
            searchBtn.click();
            driver.quit();
        }

}
