import confif.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_13_HomePage_Menu_Test extends SeleniumTestBase {

    @Test //(dependsOnGroups = "smoke") - zależność jeden od drugiego
    public void goToProcessTest() {
        new LoginPage(driver)
                .typeEmail(new Config().getEmail())
                .typePassword(new Config().getPassword())
                .submitLogin()
                .goToProcesses()
                .assertProcessHeader()
                .assertProcessUrl("http://localhost:4444/Projects");
    }
    @Test
    public void goToCharacteristic() {
        new LoginPage(driver)
                .typeEmail(new Config().getEmail())
                .typePassword(new Config().getPassword())
                .submitLogin()
                .goToCharacteristic()
                .assertProcessHeader()
                .assertProcessUrl("http://localhost:4444/Characteristics");
    }

    @Test
    public void goToDashboardTest() {
        new LoginPage(driver)
                .typeEmail(new Config().getPassword())
                .typePassword("Test1!")
                .submitLogin()
                .goToDashboard()
                .assertDashboardHeader()
                .assertDashboardUrl(new Config().getApplicationUrl());
    }
}
