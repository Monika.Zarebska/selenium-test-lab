import confif.Config;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Lab_5_Test_Niepoprawnego_Logowania_Email_Test {
    @Test

    public void incorrectLoginTestWrongEmail(){
        System.setProperty("webdriver.chrome.driver", "c:/dev/driver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(new Config().getApplicationUrl());
        WebElement emailTxt = driver.findElement(By.cssSelector("#Email"));
        emailTxt.sendKeys("test.");

        WebElement passwordTxt=driver.findElement(By.cssSelector("#Password"));
        passwordTxt.sendKeys(new Config().getPassword());

        WebElement loginBtn = driver.findElement(By.cssSelector("button[type=submit]"));
        loginBtn.click();

        WebElement emailError = driver.findElement(By.cssSelector("#Email-error"));

        boolean doesErrorExists=true;

        List<WebElement> validationErrors = driver.findElements(By.cssSelector(".validation-summary-errors>ul>li"));    //
                doesErrorExists = validationErrors //( WebElement(), WebElement()
                        .stream()
                        .map (validationError ->validationError.getText()) // wez wyciagnij tekst ("The Email field is not a valid e-mail adress."));//"The Emaild field..." i "The Password fireld is req..."
                        .anyMatch(error->error.equals("The Email field is not a valid e-mail address.")); //czy którykolwiek z el listy spelnia warunek.
        Assert.assertTrue(doesErrorExists);

        driver.quit();


    };

}
