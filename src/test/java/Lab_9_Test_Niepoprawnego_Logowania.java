import confif.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_9_Test_Niepoprawnego_Logowania extends SeleniumTestBase {

    @Test
    public void incorrectLoginTest() {

        new LoginPage(driver)
                .typeEmail("test2@test.com\"")
                .typePassword(new Config().getPassword())
                .submitLoginWithFailure()
                .assertLoginErrorsIsShown("The Email field is not a valid e-mail address.");
    }
}
