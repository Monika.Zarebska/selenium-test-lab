import confif.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_8_Test_Poprawnego_logowania_POP_Test extends SeleniumTestBase {

//    @Test
//    public void correctLoginTest() {
//
//        LoginPage loginPage =new LoginPage(driver);
//        loginPage.typeEmail("test@test.com");
//        loginPage.typePassword("Test1!");
//
//        HomePage homePage =loginPage.submitLogin();
//        homePage.assertWelcomeElementIsShown();
//    }

    @Test
    public void correctLoginTestWithChaining() {
        new LoginPage(driver).typeEmail(new Config().getEmail())
                .typePassword(new Config().getPassword())
                .submitLogin()
                .assertWelcomeElementIsShown();
    }
}
