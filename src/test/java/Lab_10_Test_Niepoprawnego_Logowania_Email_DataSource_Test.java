import confif.Config;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_10_Test_Niepoprawnego_Logowania_Email_DataSource_Test extends SeleniumTestBase {

    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"@test"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectEmailTest(String wrongEmail) {//test uruchomi się trzy razy, String bo musi sie zgadzac z Dataproviderem, a wrongEmail to dowolna nazwa zmiennej

        new LoginPage(driver)
                .typeEmail("test2@test.com\"")
                .typePassword(new Config().getPassword())
                .submitLoginWithFailure()
                .assertLoginErrorsIsShown("The Email field is not a valid e-mail address.");
    }
}
