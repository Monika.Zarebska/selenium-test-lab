import confif.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Lab_16_Test_Tworzenia_Procesu extends SeleniumTestBase{

    @Test
    public void newProcessInApplication() {
        String processName = UUID.randomUUID().toString().substring(0, 10);

        new LoginPage(driver)
                .typeEmail(new Config().getEmail())
                .typePassword(new Config().getPassword())
                .submitLogin()
                .goToProcesses()
                .addNewProcess()
                .insertProcessName(processName)
                .clickCreateBtn()
                .assertProcess(processName,"","");
    }
}

