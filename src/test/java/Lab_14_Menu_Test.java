import confif.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_14_Menu_Test extends SeleniumTestBase {

    @Test// (invocationCount = 10)
    public void menuTest(){

        new LoginPage(driver)
                .typeEmail(new Config().getEmail())
                .typePassword(new Config().getPassword())
                .submitLogin()
                .goToProcesses()
                    .assertProcessHeader()
                    .assertProcessUrl("http://localhost:4444/Projects")
                .goToCharacteristic()
                    .assertProcessUrl("http://localhost:4444/Characteristics")
                    .assertProcessHeader()
                .goToDashboard()
                    .assertDashboardUrl(new Config().getApplicationUrl())
                    .assertDashboardHeader();
    }
}
