import confif.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_17_Test_Negatywnego_Tworzenia_Procesu extends SeleniumTestBase {

    @Test
    public void newProcessInApplication() {
        String shortProcessName = "ab";
        String errorText ="The field Name must be a string with a minimum length of 3 and a maximum length of 30.";

        new LoginPage(driver)
                .typeEmail(new Config().getEmail())
                .typePassword(new Config().getPassword())
                .submitLogin()
                .goToProcesses()
                .addNewProcess()
                .insertProcessName(shortProcessName)
                .clickCreateBtnWithErrors()
                    .assertsErrorOnPage(errorText)
                .backToListBtn()
                .assertProcessIsNotShown(shortProcessName);
    }
}
