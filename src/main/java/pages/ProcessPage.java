package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class ProcessPage extends HomePage{

    public ProcessPage(WebDriver driver) {
        super(driver);
    }

    @FindBy (css = ".page-title h3")
    private WebElement pageHeader;

    @FindBy (linkText = "Add new process")
    private WebElement addNewProcessBtn;

    public ProcessPage assertProcessHeader(){
        Assert.assertTrue(pageHeader.isDisplayed());
        Assert.assertEquals(pageHeader.getText(),"Processes");

        return this;
    }

    public ProcessPage assertProcessUrl(String expUrl){
        Assert.assertEquals(driver.getCurrentUrl(),expUrl);
        return this;
    }

    public CreateProcess addNewProcess(){
            addNewProcessBtn.click();
            return new CreateProcess(driver);
    }
    private final String GENERIC_PROCESS_ROW_XPATH = "//td[text()='%s']/..";

    public ProcessPage assertProcess(String expName, String expDescription, String expNotes) {
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, expName);
        WebElement processRow = driver.findElement(By.xpath(processXpath));
        String actDescription = processRow.findElement(By.xpath("./td[2]")).getText();
        String actNotes = processRow.findElement(By.xpath("./td[3]")).getText();
        Assert.assertEquals(actDescription, expDescription);
        Assert.assertEquals(actNotes, expNotes);
        return this;
    }

    public ProcessPage assertProcessIsNotShown(String processName){
        String processXpath =String.format(GENERIC_PROCESS_ROW_XPATH,processName);
        List<WebElement> process =driver.findElements(By.xpath(processXpath));
        Assert.assertEquals(process.size(), 0);

        return this;
    }
}