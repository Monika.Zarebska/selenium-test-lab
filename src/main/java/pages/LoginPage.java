package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class LoginPage {

    protected WebDriver driver;


    public LoginPage(WebDriver driver) { //konstruktor
        this.driver = driver;   //nowy driver ten co jest wyżej
        PageFactory.initElements(driver, this); // Widzi elementy FindBy

    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(css = "#Password")
    private WebElement passwordTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement loginBtn;

    @FindBy(css = ".validation-summary-errors>ul>li")
    public List<WebElement> loginErrors;

    @FindBy(id= "Email-error")
    public WebElement emailError;

    @FindBy (css = "a[href *=Register]")
    private WebElement registerBtn;


    public LoginPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public LoginPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }

    public HomePage submitLogin() {
        loginBtn.click();
        return new HomePage(driver);
    }

    public LoginPage submitLoginWithFailure() {
        loginBtn.click();
        return this;
    }

    public void assertLoginErrorsIsShown(String expectedError) {
        boolean doesErrorExist = loginErrors
                .stream()
                .anyMatch(error -> error
                        .getText()
                        .equals(expectedError));
        Assert.assertTrue(doesErrorExist, "Error does not exist");//message gdy sie error nie pojawi

    }

    public CreateAccountPage register() {
        registerBtn.click();
        return new CreateAccountPage(driver);
    }


}






