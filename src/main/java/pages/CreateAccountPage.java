package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CreateAccountPage {

    protected WebDriver driver;

    public CreateAccountPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        Assert.assertTrue(driver.getCurrentUrl().contains("Account/Register"));
    }

    @FindBy(xpath = "//h1[text()='Create Account']")
    public WebElement registerElm;

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(id = "Password")
    private WebElement PasswordTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement RegisterBtn;

    @FindBy(id= "Email-error")
    private WebElement emailError;


    public CreateAccountPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public CreateAccountPage registerButtonClick() {
        RegisterBtn.click();
        return this;
    }

    public void invalidEmailRegistration() {

        Assert.assertEquals(emailError.getText(), "The Email field is not a valid e-mail address.");
    }
}
