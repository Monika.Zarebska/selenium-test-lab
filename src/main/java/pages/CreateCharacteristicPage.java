package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CreateCharacteristicPage {

    protected WebDriver driver;

    public CreateCharacteristicPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    @FindBy(id = "ProjectId")
    private WebElement projectSlc;

    @FindBy(id = "Name")
    private WebElement nameTxt;

    @FindBy(id = "LowerSpecificationLimit")
    private WebElement lowLimit;

    @FindBy(id = "UpperSpecificationLimit")
    private WebElement upLimit;

    @FindBy(id = "HistogramBinCount")
    private WebElement histogram;

    @FindBy(css = "input[value=Create]")
    private WebElement createBtn;

    @FindBy(css = ".field-validation-error[data-valmsg-for=ProjectId]")
    private WebElement errorName;

    @FindBy(linkText = "Back to List")
    private WebElement backToListBtn;


    public CreateCharacteristicPage selectProcess(String processName) {
        new Select(projectSlc).selectByVisibleText(processName);
        return this;
    }

    public CreateCharacteristicPage typeName(String name) {
        nameTxt.clear();
        nameTxt.sendKeys(name);
        return this;
    }

    public CreateCharacteristicPage lowLimit(String lowLLmt) {
        lowLimit.clear();
        lowLimit.sendKeys(lowLLmt);
        return this;
    }

    public CreateCharacteristicPage upperLimit(String upLmt) {
        upLimit.clear();
        upLimit.sendKeys(upLmt);
        return this;
    }

    public CharacteristicPage clickCreateBtn() {
        createBtn.click();
        return new CharacteristicPage(driver);
    }

    public CreateCharacteristicPage clickCreateBtnWithError() {
        createBtn.click();
        return this;
    }

    public CharacteristicPage clickBackBtn() {
        backToListBtn.click();
        return new CharacteristicPage(driver);
    }

    public CreateCharacteristicPage assertsErrorOnPage(String expectedError) {
        Assert.assertEquals(errorName.getText(), expectedError);

        return this;
    }
}
