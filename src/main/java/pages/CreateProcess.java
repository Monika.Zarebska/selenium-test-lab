package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CreateProcess {

    protected WebDriver driver;

    @FindBy(css = "#Name")
    private WebElement procesName;

    @FindBy(css = "input[value=Create]")
    private WebElement createBtn;

    @FindBy(css = ".field-validation-error[data-valmsg-for=Name]")
    private WebElement errorName;

    @FindBy(linkText = "Back to List")
    private WebElement backToList;

    public CreateProcess(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public CreateProcess insertProcessName(String processName) {
        procesName.clear();
        procesName.sendKeys(processName);
        return this;
    }

    public ProcessPage clickCreateBtn() {
        createBtn.click();
        return new ProcessPage(driver);
    }

    public CreateProcess clickCreateBtnWithErrors() {
        createBtn.click();
        return this;
    }

    public CreateProcess assertsErrorOnPage(String expectedError) {
        Assert.assertEquals(errorName.getText(), expectedError);

        return this;
    }

    public ProcessPage backToListBtn() {
        backToList.click();
        return new ProcessPage(driver);
    }

}


