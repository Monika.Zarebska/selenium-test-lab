package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class HomePage {

    protected WebDriver driver;

    @FindBy(css = ".profile_info>h2")
    private WebElement welcomeElm;

    @FindBy(css = ".menu-workspace")
    private WebElement workspaceNav;

    @FindBy(xpath = "//*[@class='menu-workspace']/..//ul")
    private WebElement workspaceChildMenu;

    @FindBy(css = ".menu-home")
    private WebElement homeNav;

    @FindBy(css = "a[href$=Projects]")
    private WebElement prosessesMenu;

    @FindBy(css = "a[href$=Characteristics]")
    private WebElement characteristic;

    @FindBy(linkText = "Dashboard")
    private WebElement dashboard;


    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public HomePage assertWelcomeElementIsShown() {
        Assert.assertTrue(welcomeElm.isDisplayed(), "Welcome element is not shown.");
        Assert.assertTrue(welcomeElm.getText().contains("Welcome"), "Welcome element text: '" + welcomeElm.getText() + "' does not contain word 'Welcome'");
        return this;
    }

    private boolean isParentExpanded(WebElement menuLink) {
        WebElement parent = menuLink.findElement(By.xpath("./.."));

        return parent.getAttribute("class").contains("active");
    }

    public ProcessPage goToProcesses() {
        if (!isParentExpanded(workspaceNav)) {
            workspaceNav.click();
        }
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOf(workspaceChildMenu));
        wait.until(ExpectedConditions.elementToBeClickable(prosessesMenu));
        prosessesMenu.click();

        return new ProcessPage(driver);
    }

    public CharacteristicPage goToCharacteristic() {
        if (!isParentExpanded(workspaceNav)) {
            workspaceNav.click();
        }
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOf(workspaceChildMenu));
        wait.until(ExpectedConditions.elementToBeClickable(characteristic));
        characteristic.click();

        return new CharacteristicPage(driver);
    }

    public DashboardPage goToDashboard() {
        if (!isParentExpanded(homeNav)) {
            homeNav.click();
        }
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(dashboard));
        dashboard.click();

        return new DashboardPage(driver);
    }

}
