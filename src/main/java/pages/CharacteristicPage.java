package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class CharacteristicPage extends HomePage {

    public CharacteristicPage(WebDriver driver) {
        super(driver);

    }

    @FindBy(css = ".page-title h3")
    private WebElement pageHeader;

    @FindBy(linkText = "Add new characteristic")
    private WebElement addNewCharactBtn;

    public CharacteristicPage assertProcessHeader() {
        Assert.assertTrue(pageHeader.isDisplayed());
        Assert.assertEquals(pageHeader.getText(), "Characteristics");

        return this;
    }

    public CharacteristicPage assertProcessUrl(String expUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), expUrl);
        return this;
    }

    public CreateCharacteristicPage clickAddCharacteristic() {
        addNewCharactBtn.click();
        return new CreateCharacteristicPage(driver);
    }

    private String GENERIC_CHARACTERISTIC_ROW_XPATH = "//td[text()='%s']/..";

    public CharacteristicPage assertCharacteristic(String expName, String expLsl, String expUsl, String expBinCount) {
        String characteristicXpath = String.format(GENERIC_CHARACTERISTIC_ROW_XPATH, expName);
        WebElement characteristicRow = driver.findElement(By.xpath(characteristicXpath));

        String actLsl = characteristicRow.findElement(By.xpath("./td[3]")).getText();
        String actUsl = characteristicRow.findElement(By.xpath("./td[4]")).getText();
        String actBinCount = characteristicRow.findElement(By.xpath("./td[5]")).getText();

        Assert.assertEquals(actLsl, expLsl);
        Assert.assertEquals(actUsl, expUsl);
        Assert.assertEquals(actBinCount, expBinCount);

        return this;

    }
}
